# GestureControl

The computer vision gesture control project.

This is the git repository for the OpenCV-Mouse control

rip_movementDebug.py is the most up to date

The requirements for the python scripts are as follow:
- [x] TensorFlow-GPU==2.2.0
- [x] Keras==2.4.3
- [x] ImgAug==0.2.6
- [x] OpenCV==4.3.0
- [x] Weights: [```Download the pre-trained weights```](https://mega.nz/#F!6stCxY5b!oB-3279KkhfhRULQFQO7yQ) files of the unified gesture recognition and fingertip detection model and put the ```weights``` folder in the working directory.
- [x] numpy==1.18.5
- [x] pyautogui==0.9.50


The sources for this program are from these github repos:
    https://github.com/MahmudulAlam/Unified-Gesture-and-Fingertip-Detection
    https://github.com/paolobarbolini/OpenCvMouseControl
    
Guide for tensorflow-gpu and nvidia driver installation
    https://www.youtube.com/watch?v=qrkEYf-YDyI&list=LLvoL5Z4vDm3vbhsnkPa2pGA&index=2&t=644s